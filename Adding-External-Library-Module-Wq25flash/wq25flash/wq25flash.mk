
WQ25LIB := ./wq25flash
# List of all the WQ25FLASH device files.
WQ25FLASHSRC := $(CHIBIOS)/os/ex/wq25flash/src/wq25flash.c

# Required include directories
WQ25FLASHINC := $(CHIBIOS)/os/ex/wq25flash/include

# Shared variables
ALLCSRC += $(WQ25FLASHSRC)
ALLINC  += $(WQ25FLASHINC)