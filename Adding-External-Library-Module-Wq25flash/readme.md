/*In this we are going to show that how to add external library module in chibios and use it anywhwere in the programe */

Steps to create module:-
  - Create a  module lets say wq5flash.
  - Inside it create include and src directory.
  - Add wq25flash.h file in include directory.
  - Add wq25flash.c file in src directory.
  - Create wq25flash.mk file, it contains the list of include and src file as shown below :-

      # List of all the WQ25FLASH device files.
        WQ25FLASHSRC := $(CHIBIOS)/os/ex/wq25flash/src/wq25flash.c

      # Required include directories
        WQ25FLASHINC := $(CHIBIOS)/os/ex/wq25flash/include

      # Shared variables
        ALLCSRC += $(WQ25FLASHSRC)
        ALLINC  += $(WQ25FLASHINC)
    

- After creating the mk file inside modules then we can easily include our module in main makefile anywhere in the program.