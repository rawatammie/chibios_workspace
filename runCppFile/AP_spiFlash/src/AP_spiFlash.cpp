/*
 * wq25_custom.cpp
 *
 *  Created on: 25-Aug-2021
 *      Author: kunal
 */


#include "AP_spiFlash.h"

void AP_spiFlash::SPI2_Recv( uint8_t* dt, uint16_t cnt )
{
 // dev->transfer(nullptr, 0, recv, recv_data);
 spiReceive(&SPID2, cnt, dt);
}

void AP_spiFlash::SPI2_Send( uint8_t* dt, uint16_t cnt)
{
 // dev->transfer(tran, tran_data, nullptr, 0);
  spiSend(&SPID2, cnt, dt);
}

void AP_spiFlash:: W25_Reset(void)
{
     uint8_t tx_buf[10];
//   dev-> set_chip_select(true);
//   tx_buf[0] = reset1;
//   tx_buf[1] = reset2;
//   SPI2_Send(tx_buf, 2);
//   dev-> set_chip_select(false);
    tx_buf[0] = reset1;
    tx_buf[1] = reset2;
    cs_reset();
    SPI2_Send(tx_buf, 2);
    cs_set();



}


void AP_spiFlash::WriteEnable_flash(void)
{
    uint8_t tx_buf[1];
    tx_buf[0] = JEDEC_WRITE_ENABLE;
    cs_reset();
    SPI2_Send(tx_buf,1);
    cs_set();

}


void AP_spiFlash::W25_Read_Data(uint32_t addr, uint8_t* data, uint32_t sz)
{
  uint8_t tx_buf[4];
  tx_buf[0] = JEDEC_READ_DATA;
  tx_buf[1] = (addr >> 16) & 0xFF;
  tx_buf[2] = (addr >> 8) & 0xFF;
  tx_buf[3] = addr & 0xFF;
  cs_reset();
  SPI2_Send(tx_buf, 4);
  SPI2_Recv(data, sz);
  cs_set();

}


void AP_spiFlash::W25_Write_Data(uint32_t addr, uint8_t* data, uint32_t sz)
{
   uint8_t tx_buf[4];
   
   tx_buf[0] = JEDEC_PAGE_WRITE ;
   tx_buf[1] = (addr >> 16) & 0xFF;
   tx_buf[2] = (addr >> 8) & 0xFF;
   tx_buf[3] = addr & 0xFF;
   WriteEnable_flash();
   cs_reset();
   SPI2_Send(tx_buf, 4);
   SPI2_Send(data, sz);
   cs_set();
}

uint32_t AP_spiFlash::W25_Read_ID(void)
{
  uint8_t tx_buf[10];
  uint8_t dt[4];
  tx_buf[0] = JEDEC_DEVICE_ID;
  //dev-> set_chip_select(true);
  cs_reset();
  SPI2_Send(tx_buf, 1);
  SPI2_Recv(dt,3);
  //dev-> set_chip_select(false);
  cs_set();
  return (dt[0] << 16) | (dt[1] << 8) | dt[2];
}

void AP_spiFlash::erase_sector4KB(uint32_t addr)
{
    uint8_t tx_buf[4];
    tx_buf[0] = JEDEC_SECTOR4_ERASE;
    //tx_buf[1] = (addr >> 24) & 0xff;
    tx_buf[1] = (addr >> 16) & 0xFF;
    tx_buf[2] = (addr >> 8) & 0xFF;
    tx_buf[3] = addr & 0xFF;
    WriteEnable_flash();
    cs_reset();
    SPI2_Send(tx_buf,4);
    cs_set();

    
}

void AP_spiFlash::erase_sector32KB(uint32_t addr)
{
    uint8_t tx_buf[4];
    tx_buf[0] = SectErase4KB;
    tx_buf[1] = (addr >> 16) & 0xFF;
    tx_buf[2] = (addr >> 8) & 0xFF;
    tx_buf[3] = addr & 0xFF;
    cs_reset();
    SPI2_Send(tx_buf,4);
    cs_set();

    // WITH_SEMAPHORE(dev_sem);
    // WriteEnable_flash();
    // dev-> set_chip_select(true);
    // SPI2_Send(tx_buf,5);
    // dev-> set_chip_select(false);
}

void AP_spiFlash::erase_sector64KB(uint32_t addr)
{

    uint8_t tx_buf[5];
    tx_buf[0] = JEDEC_BLOCK64_ERASE;
    tx_buf[1] = (addr >> 24) & 0xff;
    tx_buf[2] = (addr >> 16) & 0xFF;
    tx_buf[3] = (addr >> 8) & 0xFF;
    tx_buf[4] = addr & 0xFF;
    cs_reset();
    SPI2_Send(tx_buf,5);
    cs_set();



    // WITH_SEMAPHORE(dev_sem);
    // WriteEnable_flash();
    // dev-> set_chip_select(true);
    // SPI2_Send(tx_buf,5);
    // dev-> set_chip_select(false);
}

void AP_spiFlash::chip_erase(void)
{
    uint8_t tx_buf[1];
    // WITH_SEMAPHORE(dev_sem);
    // WriteEnable_flash();
    // dev-> set_chip_select(true);
    // tx_buf[0] = JEDEC_BULK_ERASE;
    // SPI2_Send(tx_buf,1);
    // dev-> set_chip_select(false);
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = JEDEC_BULK_ERASE;
    SPI2_Send(tx_buf,1);
    cs_set();


}


void AP_spiFlash::WriteSR(uint8_t SR_address, uint8_t SR_data)
{
    uint8_t tx_buf[10];
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = SR_address;
    tx_buf[1] = SR_data;
    SPI2_Send(tx_buf,2);
    cs_set();
}

uint8_t AP_spiFlash::ReadSR(uint8_t SR_address)
{
    uint8_t tx_buf[10];
    uint8_t RSR[1] = {0};
    //dev-> set_chip_select(true);
    cs_reset();
    tx_buf[0] =  SR_address;
    SPI2_Send(tx_buf,1);
    SPI2_Recv(RSR,1);
    //dev-> set_chip_select(false);
    cs_set();

    return RSR[0];
}

void AP_spiFlash::wq25_init(void)
{
	    chThdSleepMilliseconds(100);
        W25_Reset();
        chThdSleepMilliseconds(100);
}

// bool AP_spiFlash::wq25_getSectorCount(void)
// {
//             WaitReady();
// 	    WITH_SEMAPHORE(dev_sem);

//             uint8_t cmd = JEDEC_DEVICE_ID;
// 	    dev->transfer(&cmd, 1, buffer, 4);

// 	    uint32_t id = buffer[0] << 16 | buffer[1] << 8 | buffer[2];

// 	    uint32_t blocks = 0;

// 	    switch (id) {
// 	    case JEDEC_ID_WINBOND_W25Q16:
// 	    case JEDEC_ID_MICRON_M25P16:
// 	        blocks = 32;
// 	        df_PagePerBlock = 256;
// 	        df_PagePerSector = 16;
// 	        use_32bit_address = false;
// 	        break;
// 	    case JEDEC_ID_WINBOND_W25Q32:
// 	    case JEDEC_ID_MACRONIX_MX25L3206E:
// 	        blocks = 64;
// 	        df_PagePerBlock = 256;
// 	        df_PagePerSector = 16;
// 	        use_32bit_address = false;
// 	        break;
// 	    case JEDEC_ID_MICRON_N25Q064:
// 	    case JEDEC_ID_WINBOND_W25Q64:
// 	    case JEDEC_ID_MACRONIX_MX25L6406E:
// 	        blocks = 128;
// 	        df_PagePerBlock = 256;
// 	        df_PagePerSector = 16;
// 	        use_32bit_address = false;
// 	        break;
// 	    case JEDEC_ID_MICRON_N25Q128:
// 	    case JEDEC_ID_WINBOND_W25Q128:
// 	    case JEDEC_ID_CYPRESS_S25FL128L:
// 	        blocks = 256;
// 	        df_PagePerBlock = 256;
// 	        df_PagePerSector = 16;
// 	        use_32bit_address = false;
// 	        break;
// 	    case JEDEC_ID_WINBOND_W25Q256:
// 	    case JEDEC_ID_MACRONIX_MX25L25635E:
// 	        blocks = 512;
// 	        df_PagePerBlock = 256;
// 	        df_PagePerSector = 16;
// 	        use_32bit_address = true;
// 	        break;
// 	    default:
// 	        hal.scheduler->delay(2000);
// 	        hal.console->printf("Unknown SPI Flash 0x%08lx\n", id);
// 	        return false;
// 	    }

// 	    df_PageSize = 256;
// 	    df_NumPages = blocks * df_PagePerBlock;
// 	    erase_cmd = JEDEC_BLOCK64_ERASE;

// 	    hal.scheduler->delay(2000);
// 	    hal.console->printf("SPI Flash 0x%08lx found pages=%lu erase=%luk\n",id, df_NumPages, (df_PagePerBlock * (uint32_t)df_PageSize)/1024);
// 	    return true;
// }


// /*
//   wait for busy flag to be cleared
//  */
// void AP_spiFlash::WaitReady()
// {
//     if (flash_died) {
//         return;
//     }

//     uint32_t t = AP_HAL::millis();
//     while (Busy()) {
//         hal.scheduler->delay_microseconds(100);
//         if (AP_HAL::millis() - t > 5000) {
//             hal.console->printf("DataFlash: flash_died\n");
//             flash_died = true;
//             break;
//         }
//     }
// }

// bool AP_spiFlash::Busy()
// {
//     return (ReadStatusReg() & (JEDEC_STATUS_BUSY | JEDEC_STATUS_SRP0)) != 0;
// }


// // Read the status register
// uint8_t AP_spiFlash::ReadStatusReg()
// {
//     WITH_SEMAPHORE(dev_sem);
//     uint8_t cmd = JEDEC_READ_STATUS;
//     uint8_t status;
//     dev->transfer(&cmd, 1, &status, 1);
//     return status;
// }

// void AP_spiFlash::Enter4ByteAddressMode(void)
// {
//     WITH_SEMAPHORE(dev_sem);

//     const uint8_t cmd = 0xB7;
//     dev->transfer(&cmd, 1, NULL, 0);
// }

/*
  send a command with an address
*/
// void AP_spiFlash::send_command_addr(uint8_t command, uint32_t PageAdr)
// {
//     uint8_t cmd[5];
//     cmd[0] = command;
//     if (use_32bit_address) {
//         cmd[1] = (PageAdr >> 24) & 0xff;
//         cmd[2] = (PageAdr >> 16) & 0xff;
//         cmd[3] = (PageAdr >>  8) & 0xff;
//         cmd[4] = (PageAdr >>  0) & 0xff;
//     } else {
//         cmd[1] = (PageAdr >> 16) & 0xff;
//         cmd[2] = (PageAdr >>  8) & 0xff;
//         cmd[3] = (PageAdr >>  0) & 0xff;
//     }

//     dev->transfer(cmd, use_32bit_address?5:4, NULL, 0);
// }


