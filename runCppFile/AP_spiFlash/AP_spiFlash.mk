# List of all the AP_spiFlash device files.
FLASHSRC := $(CHIBIOS)/os/ex/AP_spiFlash/src/AP_spiFlash.cpp

# Required include directories
FLASHINC := $(CHIBIOS)/os/ex/AP_spiFlash/include

# Shared variables
ALLCPPSRC += $(FLASHSRC)
ALLINC  += $(FLASHINC)