/*
 * wq25_custom.h
 *
 *  Created on: 25-Aug-2021
 *      Author: kunal
 */
#ifndef AP_SPIFLASH_H
#define AP_SPIFLASH_H
#include "hal.h"
#define cs_set() palSetPad(GPIOC, GPIOC_PIN1)
#define cs_reset() palClearPad(GPIOC, GPIOC_PIN1)

#define JEDEC_WRITE_ENABLE           0x06
#define JEDEC_WRITE_DISABLE          0x04
#define JEDEC_READ_STATUS            0x05
#define JEDEC_WRITE_STATUS           0x01
#define JEDEC_READ_DATA              0x03
#define JEDEC_FAST_READ              0x0b
#define JEDEC_DEVICE_ID              0x9F
#define JEDEC_PAGE_WRITE             0x02
#define ReadSR1                      0x05
#define ReadSR2                      0x35
#define ReadSR3                      0x15

#define JEDEC_BULK_ERASE             0xC7
#define JEDEC_SECTOR4_ERASE          0x20 // 4k erase
#define JEDEC_BLOCK32_ERASE          0x52 // 32K erase
#define JEDEC_BLOCK64_ERASE          0xD8 // 64K erase

#define JEDEC_STATUS_BUSY            0x01
#define JEDEC_STATUS_WRITEPROTECT    0x02
#define JEDEC_STATUS_BP0             0x04
#define JEDEC_STATUS_BP1             0x08
#define JEDEC_STATUS_BP2             0x10
#define JEDEC_STATUS_TP              0x20
#define JEDEC_STATUS_SEC             0x40
#define JEDEC_STATUS_SRP0            0x80

/*
  flash device IDs taken from betaflight flash_m25p16.c

  Format is manufacturer, memory type, then capacity
*/
#define JEDEC_ID_MACRONIX_MX25L3206E   0xC22016
#define JEDEC_ID_MACRONIX_MX25L6406E   0xC22017
#define JEDEC_ID_MACRONIX_MX25L25635E  0xC22019
#define JEDEC_ID_MICRON_M25P16         0x202015
#define JEDEC_ID_MICRON_N25Q064        0x20BA17
#define JEDEC_ID_MICRON_N25Q128        0x20ba18
#define JEDEC_ID_WINBOND_W25Q16        0xEF4015
#define JEDEC_ID_WINBOND_W25Q32        0xEF4016
#define JEDEC_ID_WINBOND_W25Q64        0xEF4017
#define JEDEC_ID_WINBOND_W25Q128       0xEF4018
#define JEDEC_ID_WINBOND_W25Q256       0xEF4019
#define JEDEC_ID_CYPRESS_S25FL128L     0x016018
#define reset1 0x66
#define reset2 0x99
#define SectErase4KB 0x20


#define block0_addr 0x000000
#define block1_addr 0x020000
#define block2_addr 0x030000
#define block3_addr 0x040000
#define block4_addr 0x050000
#define block5_addr 0x060000
#define block6_addr 0x070000
#define block7_addr 0x080000
#define block8_addr 0x090000
#define block9_addr 0x0A0000
#define block10_addr 0x0B0000


class AP_spiFlash {
public:

    AP_spiFlash() { }

    /* Do not allow copies */
    AP_spiFlash(const AP_spiFlash &other) = delete;
    AP_spiFlash &operator=(const AP_spiFlash&) = delete;

    // get singleton instance
    // static AP_spiFlash *get_singleton(void) {
    //     return _singleton;
    // }


	void wq25_init();
	//bool wq25_getSectorCount();

	void W25_Reset ();
	void WriteEnable_flash();
	//void W25_Read_Data(uint32_t addr, uint8_t* data, uint32_t sz);
	//uint8_t* W25_Read_Data(uint32_t addr, uint8_t* data, uint32_t sz);
	void W25_Read_Data(uint32_t addr, uint8_t* data, uint32_t sz);
	void W25_Write_Data(uint32_t addr, uint8_t* data, uint32_t sz);
	uint32_t W25_Read_ID();
	void erase_sector4KB(uint32_t addr);
	void erase_sector32KB(uint32_t addr);
	void erase_sector64KB(uint32_t addr);
	void chip_erase();
	void WriteSR(uint8_t SR_address, uint8_t SR_data);
	uint8_t ReadSR(uint8_t SR_address);
        //bool              Busy();

//protected:

        // number of bytes in a page
	//uint32_t df_PageSize;    
        // number of pages in a (generally 64k) block
	//uint16_t df_PagePerBlock;
	// number of pages in a (generally 4k) sector
	//uint16_t df_PagePerSector;
	// number of pages on the chip
	//uint32_t df_NumPages;

        //static const uint16_t page_size_max = 256;
	//uint8_t *buffer;



 
        //static AP_spiFlash *_singleton;

	// AP_HAL::OwnPtr<AP_HAL::SPIDevice> dev;
	// AP_HAL::Semaphore *dev_sem;

    void SPI2_Recv(uint8_t* dt, uint16_t cnt );
	void SPI2_Send(uint8_t* dt, uint16_t cnt);

    //void send_command_addr(uint8_t cmd, uint32_t address);

	//bool flash_died;
        //void              WaitReady();
        //bool              Busy();
        //uint8_t           ReadStatusReg();
       // void              Enter4ByteAddressMode(void);

	//uint32_t erase_start_ms;
	//uint8_t erase_cmd;
	//bool use_32bit_address;

};

// namespace AP {
//     AP_spiFlash &spiflash();
// };

#endif