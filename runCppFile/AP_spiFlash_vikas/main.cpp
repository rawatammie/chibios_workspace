/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "usbcfg.h"
#include "chprintf.h"
#include "AP_spiFlash.h"
#define cls(chp)  chprintf(chp, "\033[2J\033[1;1H")



/*===========================================================================*/
/* SPI driver related.                                                       */
/*===========================================================================*/

#define SPI_LOOPBACK

/*
 * Maximum speed SPI configuration (27MHz, CPHA=0, CPOL=0, MSb first).
 */
static const SPIConfig hs_spicfg = {
  FALSE,
  NULL,
  GPIOC,         /*cs port */
  GPIOC_PIN1,   /* cs pin1*/
  SPI_CR1_BR_0 | SPI_CR1_BR_1 |SPI_CR1_SSM |SPI_CR1_SSI,  /*CONFIGURING CR1 REGISTER*/
  0
};

/*
 * SPI TX and RX buffers.
 * Note, the buffer are aligned to a 32 bytes boundary b  ecause limitations
 * imposed by the data cache. Note, this is GNU specific, it must be
 * handled differently for other compilers.
 */

AP_spiFlash flash;
uint8_t rx_buf[1025];
char *data  = (char*)"The STM32 H7-series is a group of high performance STM32 microcontroller ";
uint8_t* tran_buff = (uint8_t*)data;

uint8_t x ,y,z;
static BaseSequentialStream* chp = (BaseSequentialStream*)&SDU1;

static THD_WORKING_AREA(spi_thread_1_wa, 1000);
static THD_FUNCTION(spi_thread_1, p) {

  (void)p;
  chRegSetThreadName("SPI thread 1");
  while (true) {


    /* Bush acquisition and SPI reprogramming.*/
    spiAcquireBus(&SPID2);
    spiStart(&SPID2, &hs_spicfg);

//    x = flash.ReadSR(ReadSR1);
//    y = flash.ReadSR(ReadSR2);
//    z = flash.ReadSR(ReadSR3);
//    chThdSleepMilliseconds(100);

    flash.erase_sector4KB(block1_addr);
    //flash.chip_erase();
    chThdSleepMilliseconds(1000);
    flash.W25_Write_Data(block1_addr,tran_buff,50);
    chThdSleepMilliseconds(100);
    //chprintf(chp,"Tx Data is = %s \r\n",data);
    //chThdSleepMilliseconds(100);
    flash.W25_Read_Data(block1_addr,rx_buf,50);
    chThdSleepMilliseconds(100);
    chprintf(chp,"Received Data is = %s \r\n",rx_buf);
    chThdSleepMilliseconds(1500);
   // palTogglePad(GPIOD, GPIOD_LED4);
   // chThdSleepMilliseconds(500);




    /* Releasing the bus.*/
    spiReleaseBus(&SPID2);
  }
}



int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */


  halInit();
  chSysInit();
  //flash.chip_erase();


  /* Initializes a serial-over-USB CDC driver.*/
    sduObjectInit(&SDU1);
    sduStart(&SDU1, &serusbcfg);

    /*
     * Activates the USB driver and then the USB bus pull-up on D+.
     * Note, a delay is inserted in order to not have to disconnect the cable
     * after a reset.
     */
    usbDisconnectBus(serusbcfg.usbp);
    chThdSleepMilliseconds(1000);
    usbStart(serusbcfg.usbp, &usbcfg);
    usbConnectBus(serusbcfg.usbp);

  /*
   * SPI2 I/O pins setup.
   */
  palSetLineMode(LINE_CLK_IN,
                 PAL_MODE_ALTERNATE(5) |
                 PAL_STM32_OSPEED_HIGHEST);         /* SPI2 SCK. PB10            */

  palSetLineMode(LINE_SPI2_MISO ,
                 PAL_MODE_ALTERNATE(5) |
                 PAL_STM32_OSPEED_HIGHEST);         /* SPI2 MISO. PC2              */

  palSetLineMode(LINE_PDM_OUT ,
                 PAL_MODE_ALTERNATE(5) |
                 PAL_STM32_OSPEED_HIGHEST);         /* SPI2 MOSI. PC3               */

  palSetLine(LINE_PIN1 );
  palSetLineMode(LINE_PIN1  ,
                 PAL_MODE_OUTPUT_PUSHPULL);         /* CS.    PC1             */

  /*
   * Starting the transmitter and receiver threads.
   */
  chThdCreateStatic(spi_thread_1_wa, sizeof(spi_thread_1_wa),
                    NORMALPRIO + 1, spi_thread_1, NULL);


  /*
   * Normal main() thread activity, in this demo it does nothing.
   */
  while (true) {

    chThdSleepMilliseconds(1000);
    //cls(chp);

  }
}

