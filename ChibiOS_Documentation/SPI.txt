                    THIS DOC EXPLAIN'S, HOW TO USE CHIBIOS SPI API TO COMMUNICATE WITH THE DIFFERENT DEVICES

STEP1 : -
 -> Configure the SPI by following the below steps:-
   a) Enable the SPI driver through the proper switch in the halconf.h. The switch name is HAL_USE_SPI.  
   b) Assign it a SPI peripheral acting on mcuconf.h. 
   c) After assigning a peripheral a new object will become available: SPID1 on SPI 1 assignation, SPID2 on SPI 2 and so on.
   d) Make SPI configuration variable and set its parameter configuration. Below is the structure of SPI configuration:-

      typedef struct {
         #if (SPI_SUPPORTS_CIRCULAR == TRUE) || defined(__DOXYGEN__)
         /**
          * @brief   Enables the circular buffer mode.
          */
          bool                      circular;
         #endif

         /**
           * @brief Operation complete callback or @p NULL.
         */
          spicallback_t             end_cb;

         #if (SPI_SELECT_MODE == SPI_SELECT_MODE_LINE) || defined(__DOXYGEN__)
           /**
            * @brief The chip select line.
            */
           ioline_t                  ssline;
         #endif

         #if (SPI_SELECT_MODE == SPI_SELECT_MODE_PORT) || defined(__DOXYGEN__)
           /**
            * @brief The chip select port.
            */
           ioportid_t                ssport;

           /**
            * @brief The chip select port mask.
            */

           ioportmask_t              ssmask;
         #endif

         #if (SPI_SELECT_MODE == SPI_SELECT_MODE_PAD) || defined(__DOXYGEN__)
           /**
            * @brief The chip select port.
            */

           ioportid_t                ssport;
          /**
           * @brief The chip select pad number.
           */

          uint_fast8_t              sspad;
        #endif

          /* End of the mandatory fields.*/
          /**
           * @brief SPI CR1 register initialization data.
           */

          uint16_t                  cr1;
          /**
           * @brief SPI CR2 register initialization data.
           */

          uint16_t                  cr2;
    } SPIConfig;

    Example:-

    static const SPIConfig hs_spicfg = {
         FALSE,
         NULL,
         GPIOC,         /*cs port */
         GPIOC_PIN1,   /* cs pin1*/
         SPI_CR1_BR_0 | SPI_CR1_BR_1 |SPI_CR1_SSM |SPI_CR1_SSI,  /*CONFIGURING CR1 REGISTER*/
         0
    };

     Note:- STM32F407 uses SPI V1 so we have used SPI V1 configuratios , as other microntroller's uses SPI V2  and SPI V3 so they used their respective SPI configurations.

STEP2:-
-> After configuration, in the main function we intialize halInit() it initializes the configured device drivers
   and performs the board-specific initializations.

-> Then we initialize the SPI communication using  SPIStart(&SPID2, &SPIcfg) function. It receives as usual two parameters: a pointer to the driver and a pointer to its configuration.

-> Setup SPI I/O pins by using the following function:-
    palSetLineMode(line,mode)

    First parameter is line. line is a smart way to encode the information of both port and pad in a single identifier like:-
     #define LINE_ARD_A2                 PAL_LINE(GPIOA, 4U)
    
    Second parameter is gpio mode. gpio has 4 modes :-
    1) Input mode
    2) Output mode 
    3) Analog mode
    4) Alternate mode

    Here we will show the example of palSetLineMode(line,mode) function in alternate mode:-
     palSetLineMode(LINE_CLK_IN,PAL_MODE_ALTERNATE(5) |PAL_STM32_OSPEED_HIGHEST);         /* SPI2 SCK. PB10  */

     palSetLineMode(LINE_SPI2_MISO ,PAL_MODE_ALTERNATE(5) |PAL_STM32_OSPEED_HIGHEST);     /* SPI2 MISO. PC2  */

     palSetLineMode(LINE_PDM_OUT ,PAL_MODE_ALTERNATE(5) |PAL_STM32_OSPEED_HIGHEST);       /* SPI2 MOSI. PC3  */
     palSetLine(LINE_PIN1 );
     palSetLineMode(LINE_PIN1  ,PAL_MODE_OUTPUT_PUSHPULL);                                /* CS. PC1         */
     
     Here,
      LINE_CLK_IN    = PAL_LINE(GPIOB, 10U)
      LINE_SPI2_MISO = PAL_LINE(GPIOC, 2U)
      LINE_PDM_OUT   = PAL_LINE(GPIOC, 3U)
      LINE_PIN1      = PAL_LINE(GPIOC, 1U)

      And mode parameter is also defined with output speed and output type configuration register parameters.


-> Communication API to exchange info between master and slave:-
   spiSend(SPIDriver *spip, size_t n, const void *txbuf)

   * @param[in] spip      pointer to the @p SPIDriver object
   * @param[in] n         number of words to send
   * @param[in] txbuf     the pointer to the transmit buffer


   spiReceive(SPIDriver *spip, size_t n, void *rxbuf)

   * @param[in] spip      pointer to the @p SPIDriver object
   * @param[in] n         number of words to receive
   * @param[out] rxbuf    the pointer to the receive buffer